'use strict';

const JSONtoCSV = (items) => {
  const replacer = (key, value) => value === null ? '' : value;
  const header = Object.keys(items[0]);
  let csv = items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
  csv.unshift(header.join(','));
  return csv.join('\r\n')
};

window.addEventListener("load", async () => {
    const vue = new Vue({
        el: "#app",
        data: {
            section: 0,
            items: []
        },
        methods: {
            show: function(id) {
                this.section = id;
            },
            remove: function(cookie) {
                vue.items = vue.items.filter(item => item.domain !== cookie.domain || item.name !== cookie.name);
                chrome.cookies.remove({url: "http://" + cookie.domain + cookie.path, name: cookie.name});
            }
        }});

  const cookies = await new Promise(resolve => chrome.cookies.getAll({}, resolve));
  let history = await new Promise(resolve => chrome.history.search({text: "", maxResults: 10000000, startTime: 0}, resolve));

  const current_tab = (await new Promise(resolve => chrome.tabs.query({currentWindow: true, active: true}, resolve)))[0];
  const domain = current_tab.url ? (new URL(current_tab.url)).hostname : null;
  vue.items = await new Promise(resolve => chrome.cookies.getAll({domain}, resolve));
  console.log(vue.items)

  history = history.map(item => ({...item, hostname: (new URL(item.url)).hostname}));//Adding hostname
  history = history.map(item => ({...item, data: new Date(item.lastVisitTime)}));//Adding human readable date

  const unique_hostnames = history.filter((item, i) => i === history.findIndex(t => t.hostname === item.hostname)).map(item => item.hostname);

  const visits = {};
  history.forEach(item => {
    visits[item.hostname] = item.visitCount + (visits.hasOwnProperty(item.hostname) ?  visits[item.hostname] : 0);
  });
  let v = Object.entries(visits);
  v = v.sort((a, b) => b[1] - a[1]);
  v = v.slice(0, 5);

  v = v.map(i => ([
      ...i,
      cookies.filter(cookie => cookie.hostOnly ? cookie.domain === i[0] : ("." + i[0]).endsWith(cookie.domain)).length
  ]));

  new Chart(
      document.getElementById("visits").getContext('2d'),
      {
        type: 'bar',
        data: {
          labels: v.map(a => a[0]),
          datasets: [{
            label: "Liczba wizyt",
            data: v.map(a => a[1]),
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });


  const cats = [
      ["Trackery", 800],
      ["Listy przedmiotów", 500],
      ["Zgody", 300],
      ["Inne", 100]
  ];
  new Chart(
      document.getElementById("categories").getContext('2d'),
      {
        type: "pie",
        data: {
          labels: cats.map(a => a[0]),
          datasets: [{
              label: "Liczba cookie",
              data: cats.map(a => a[1]),
              backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)'
              ],
              borderColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)'
              ],
              borderWidth: 1
            }]
        }
      });
});
